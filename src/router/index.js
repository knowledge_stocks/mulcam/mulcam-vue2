import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/appradio',
    name: 'AppRadio',
    component: () => import('../components/App01_radio.vue')
  },
  {
    path: '/mytodo',
    name: 'MyTodoList',
    component: () => import('../components/TodoList.vue')
  },
  {
    path: '/otherstodo',
    name: 'OtherTodoList',
    component: () => import('../views/OthersTodoList.vue')
  },
  {
    path: '/book',
    name: 'Book',
    component: () => import('../views/App02_book.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
