module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
        ? '/mulcam-vue2/'
        : '/'
}